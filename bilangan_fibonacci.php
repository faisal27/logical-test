<?php

function bilanganFibonacci() {
  $fibonacci = [];

  for ($i = 0; $i < 100; $i++) {
    if ($i < 2) {
      $bilangan = $i;
    } else {
      $bilangan = $fibonacci[$i - 1] + $fibonacci[$i - 2];
    }

    array_push($fibonacci, $bilangan);
  }

  echo implode("<br>", $fibonacci);
}

bilanganFibonacci();